pub extern crate has_a_derive;
pub use has_a_derive as derive;

pub trait HasA<T> {
    fn get_ref(&self) -> &T;
}

impl<T> HasA<T> for T {
    fn get_ref(&self) -> &T {
        self
    }
}

pub trait HasAMut<T> {
    fn get_mut(&mut self) -> &mut T;
}

impl<T> HasAMut<T> for T {
    fn get_mut(&mut self) -> &mut T {
        self
    }
}

// pub trait HasATake<T> {
//     fn take(&mut self) -> T;
// }
//
// impl<T: Default> HasATake<T> for T {
//     fn take(&mut self) -> T {
//         std::mem::take(self)
//     }
// }
