use proc_macro::TokenStream;
use quote::quote;
use syn::{parse_macro_input, Data, DeriveInput, Fields};

#[proc_macro_derive(HasA)]
pub fn has_a_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = &input.ident;
    let data = &input.data;

    let mut acc = TokenStream::new();

    match data {
        Data::Struct(data_struct) => match &data_struct.fields {
            Fields::Named(named) => {
                for field in named.named.iter() {
                    let ident = field.ident.clone().unwrap();
                    let ty = field.ty.clone();

                    let x = quote! {
                        impl has_a::HasA<#ty> for #name {
                            fn get_ref(&self) -> &#ty {
                                &self.#ident
                            }
                        }
                    };
                    acc.extend::<TokenStream>(x.into());
                }
            }
            Fields::Unnamed(unnamed) => {
                for (idx, field) in unnamed.unnamed.iter().enumerate() {
                    let ty = field.ty.clone();
                    let idx = syn::Index::from(idx);

                    let x = quote! {
                        impl has_a::HasA<#ty> for #name {
                            fn get_ref(&self) -> &#ty {
                                &self.#idx
                            }
                        }
                    };
                    acc.extend::<TokenStream>(x.into());
                }
            }
            Fields::Unit => panic!("Unit structs are not supported"),
        },
        Data::Enum(_) => panic!("Enums are not supported"),
        Data::Union(_) => panic!("Unions are not supported"),
    };

    acc.into()
}

#[proc_macro_derive(HasAMut)]
pub fn has_a_mut_derive(input: TokenStream) -> TokenStream {
    let input = parse_macro_input!(input as DeriveInput);

    let name = &input.ident;
    let data = &input.data;

    let mut acc = TokenStream::new();

    match data {
        Data::Struct(data_struct) => match &data_struct.fields {
            Fields::Named(named) => {
                for field in named.named.iter() {
                    let ident = field.ident.clone().unwrap();
                    let ty = field.ty.clone();

                    let x = quote! {
                        impl has_a::HasAMut<#ty> for #name {
                            fn get_mut(&mut self) -> &mut #ty {
                                &mut self.#ident
                            }
                        }
                    };
                    acc.extend::<TokenStream>(x.into());
                }
            }
            Fields::Unnamed(unnamed) => {
                for (idx, field) in unnamed.unnamed.iter().enumerate() {
                    let ty = field.ty.clone();
                    let idx = syn::Index::from(idx);

                    let x = quote! {
                        impl has_a::HasAMut<#ty> for #name {
                            fn get_mut(&mut self) -> &mut #ty {
                                &mut self.#idx
                            }
                        }
                    };
                    acc.extend::<TokenStream>(x.into());
                }
            }
            Fields::Unit => panic!("Unit structs are not supported"),
        },
        Data::Enum(_) => panic!("Enums are not supported"),
        Data::Union(_) => panic!("Unions are not supported"),
    };

    acc.into()
}
