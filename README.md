# Has A

This is a very simple library with two very simple traits 
to allow for some simple yet powerful generic handling.

# Example

```rust
use has_a::derive::{HasA, HasAMut};
use has_a::{HasA, HasAMut};

fn main() {
    let mut s = S {
        s: "Foo".to_string(),
        i: 12,
    };

    print_string(&s);
    print_string_and_i32(&s);
    append_to_string(&mut s, " !");
    print_string(&s);
}

#[derive(HasA, HasAMut)]
struct S {
    s: String,
    i: i32,
}

fn print_string(s: &impl HasA<String>) {
    let s: &String = s.get_ref();
    println!("{}", s);
}

fn print_string_and_i32<T: HasA<String> + HasA<i32>>(t: &T) {
    let str = HasA::<String>::get_ref(t);
    let num = HasA::<i32>::get_ref(t);
    println!("str: `{}` num: `{}`", str, num);
}

fn append_to_string(s: &mut impl HasAMut<String>, str: &str) {
    let s: &mut String = s.get_mut();
    s.push_str(str);
}
```

# License

MIT
